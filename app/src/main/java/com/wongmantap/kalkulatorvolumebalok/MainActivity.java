package com.wongmantap.kalkulatorvolumebalok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtwidth,txtlength,txtheight;
    private Button btncalculate;
    private TextView result;

    private static final String STATE_RESULT = "state_result";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_RESULT,result.getText().toString());

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtwidth = findViewById(R.id.txt_width);
        txtheight = findViewById(R.id.txt_height);
        txtlength = findViewById(R.id.txt_length);
        btncalculate = findViewById(R.id.btn_calculate);
        result = findViewById(R.id.lbl_result);

        btncalculate.setOnClickListener(this);

        if (savedInstanceState != null){
            String resultval = savedInstanceState.getString(STATE_RESULT);
            result.setText(resultval);
        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_calculate){
            String inputwidth = txtwidth.getText().toString().trim();
            String inputlength = txtlength.getText().toString().trim();
            String inputheight = txtheight.getText().toString().trim();

            boolean isEmptyFields = false;
            boolean isInvalidDouble = false;

            if (TextUtils.isEmpty(inputwidth)){
                isEmptyFields = true;
                txtwidth.setError("Silahkan Lengkapi Field ini !");
            }

            if (TextUtils.isEmpty(inputlength)){
                isEmptyFields = true;
                txtlength.setError("Silahkan Lengkapi Field ini !");
            }

            if (TextUtils.isEmpty(inputheight)){
                isEmptyFields = true;
                txtheight.setError("Silahkan Lengkapi Field ini !");
            }

            Double width = toDouble(inputwidth);
            Double length = toDouble(inputlength);
            Double height = toDouble(inputheight);

            if (length == null){
                isInvalidDouble = true;
                txtlength.setError("Field ini harus berisi angka yang benar / valid");
            }

            if (width == null){
                isInvalidDouble = true;
                txtwidth.setError("Field ini harus berisi angka yang benar / valid");
            }

            if (height == null){
                isInvalidDouble = true;
                txtheight.setError("Field ini harus berisi angka yang benar / valid");
            }

            if (!isEmptyFields && !isInvalidDouble){
                double volume = length * width * height;
                result.setText(String.valueOf(volume));
            }


        }

    }

    Double toDouble (String str){
        try{
            return Double.valueOf(str);
        }catch(NumberFormatException e){
            return null;
        }
    }

}
